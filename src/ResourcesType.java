public enum ResourcesType
{
    WATER,
    WOOD,
    STONE,
    SAND,
    TOOL,
    IRON,
    GOLD,
    COINS
}
