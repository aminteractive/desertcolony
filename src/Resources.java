public class Resources
{
    private ResourcesType name;
    private int amount;

    public Resources()
    {

    }

    public Resources(ResourcesType name, int amount)
    {
        this.name = name;
        this.amount = amount;
    }

    public ResourcesType getName()
    {
        return name;
    }

    public void setName(ResourcesType name)
    {
        this.name = name;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }
}
