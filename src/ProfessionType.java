public enum ProfessionType
{
    PEASANT,
    COLLECTOR,
    HUNTER,
    MAKER,
    HELPER,
    LEADER
}
