public enum BuildingType
{
    HOUSE,
    CLAY_MAKER,
    HUNTERS_LODGE,
    GATHERER,
    PASTURE,
    WELL
}
